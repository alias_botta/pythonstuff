# As you probably know, Sudoku is a number-placing puzzle played on a 9x9 board. The player has to fill the board in a very specific way:

# each row of the board must contain all digits from 0 to 9 (the order doesn't matter)
# each column of the board must contain all digits from 0 to 9 (again, the order doesn't matter)
# each of the nine 3x3 "tiles" (we will name them "sub-squares") of the table must contain all digits from 0 to 9.
# If you need more details, you can find them here.

# Your task is to write a program which:

# reads 9 rows of the Sudoku, each containing 9 digits (check carefully if the data entered are valid)
# outputs Yes if the Sudoku is valid, and No otherwise.
# Test your code using the data we've provided.

def sudoku_checker(table): 
    if len(table) == 9:
        numsinrow = 0
        for i in range(9):
            if len(table[i]) == 9:
                numsinrow += 1
        if numsinrow == 9:
            for i in range(9):
                rowoccurence = [0,0,0,0,0,0,0,0,0,0]
                for j in range(9):
                    rowoccurence[table[i][j]] += 1
                    temprow = rowoccurence[1:10]
                    if temprow == [1,1,1,1,1,1,1,1,1]:
                        return True
                    else:
                        return False
        else:
            return False
    else:
        return False

test1 = ["295743861","431865927","876192543","387459216","612387495","549216738","763524189","928671354","154938672"]
test2 = ["195743862","431865927","876192543","387459216","612387495","549216738","763524189","928671354","254938671"]

if sudoku_checker (test1): print("Yes")
else: print("False")

if sudoku_checker (test2): print("Yes")
else: print("False")

#not completed
