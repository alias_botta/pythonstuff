# You are already familiar with the Caesar cipher, and this is why we want you to improve the code we showed you recently.

# The original Caesar cipher shifts each character by one: a becomes b, z becomes a, and so on. Let's make it a bit harder, and allow the shifted value to come from the range 1..25 inclusive.

# Moreover, let the code preserve the letters' case (lower-case letters will remain lower-case) and all non-alphabetical characters should remain untouched.

# Your task is to write a program which:

# asks the user for one line of text to encrypt;
# asks the user for a shift value (an integer number from the range 1..25 - note: you should force the user to enter a valid shift value (don't give up and don't let bad data fool you!)
# prints out the encoded text.
# Test your code using the data we've provided.

def caesar_chiper_improved(text, shift):
    cipher = ''
    while shift >= 26: shift -= 26
    for char in text:
        if not char.isalpha():
                cipher += char
                continue
        code = ord(char) + shift
        if char.isupper(): 
            if code > ord('Z'): code = ord('A') + (code - ord('Z') -1) 
        else:#if char is lower  
            if code > ord('z'): code = ord('a') + (code - ord('z') -1) 
        cipher += chr(code)
    return cipher

print("Calculation:    " + caesar_chiper_improved("abcxyzABCxyz 123", 2))
print("Correct result: cdezabCDEzab 123\n\n\n")

print("Calculation:    " + caesar_chiper_improved("The die is cast", 25))
print("Correct result: Sgd chd hr bzrs")