# Let's play a game. We will give you two strings: one being a word (e.g., "dog") and the second being a combination of any characters.

# Your task is to write a program which answers the following question: are the characters comprising the first string hidden inside the second string?

# For example:

# if the second string is given as "vcxzxduybfdsobywuefgas", the answer is yes;
# if the second string is "vcxzxdcybfdstbywuefsas", the answer is no (as there are neither the letters "d", "o", or "g", in this order)
# Hints:

# you should use the two-argument variants of the pos() functions inside your code;
# don't worry about case sensitivity.
# Test your code using the data we've provided.

def is_contained (string1, string2):
    string1, string2 = string1.upper(), string2.upper()
    for ch in string1: 
        if ch in string2: string2 = string2.replace(ch, "", 1)
        else: return False
    return True

if is_contained("donor", "Nabucodonosor"): print("Yes")
else: print("No")

if is_contained("donut", "Nabucodonosor"): print("Yes")
else: print("No")