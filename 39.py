# Your task is to implement a class called Weeker. Yes, your eyes don't deceive you - this name comes from the fact that objects of that class will be able to store and to manipulate days of a week.

# The class constructor accepts one argument - a string. The string represents the name of the day of the week and the only acceptable values must come from the following set:

# Mon Tue Wed Thu Fri Sat Sun

# Invoking the constructor with an argument from outside this set should raise the WeekDayError exception (define it yourself; don't worry, we'll talk about the objective nature of exceptions soon). The class should provide the following facilities:

# objects of the class should be "printable", i.e. they should be able to implicitly convert themselves into strings of the same form as the constructor arguments;
# the class should be equipped with one-parameter methods called add_days(n) and subtract_days(n), with n being an integer number and updating the day of week stored inside the object in the way reflecting the change of date by the indicated number of days, forward or backward.
# all object's properties should be private;
# Complete the template we've provided in the editor and run your code and check whether your output looks the same as ours.

# Expected output
# Mon
# Thu
# Sun
# Sorry, I can't serve your request.

class WeekDayError(Exception):
    pass
	

class Weeker:
    day_dict = {
        1 : "Mon",
        2 : "Tue", 
        3 : "Wed", 
        4 : "Thu",
        5 : "Fri",
        6 : "Sat",
        7 : "Sun"
    }


    #the weirdest and less elegant solution for doing a reverse search in a dictionary :D
    day_reverse_dict = { 
        "Mon" : 1,
        "Tue" : 2, 
        "Wed" : 3, 
        "Thu" : 4,
        "Fri" : 5,
        "Sat" : 6,
        "Sun" : 7
    }
    def __init__(self, day):
        if not day in {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"}: raise WeekDayError
        else: self.__day_n = self.day_reverse_dict[day]
        self.__str__()

    def __str__(self):
        self.__day_s = self.day_dict[self.__day_n]

    def add_days(self, n):
        while n >= 7: n-=7
        self.__day_n+=n+2
        if self.__day_n > 7: self.__day_n-=7
        self.__str__()

    def subtract_days(self, n):
        while n >= 7: n-=7
        self.__day_n-=n+2
        if self.__day_n <1: self.__day_n+=7
        self.__str__()

try:
    weekday = Weeker('Mon')
    print(weekday._Weeker__day_s)
    weekday.add_days(15)
    print(weekday._Weeker__day_s)
    weekday.subtract_days(23)
    print(weekday._Weeker__day_s)
    weekday = Weeker('Monday')
except WeekDayError:
    print("Sorry, I can't serve your request.")