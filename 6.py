#We're going to find the bigger number of three
number1 = int(input("\nInsert the first number: "))
number2 = int(input("\nInsert the second number: "))
number3 = int(input("\nInsert the third number: "))

#now we're going to declare the variabile that contains the largest number
largest_number = number1

if largest_number < number2:
    largest_number = number2

if largest_number < number3: 
    largest_number = number3

#we have finished the comparation, now we're going to print the result
print("The largest number is" , largest_number)

#now we can do the trick with the max prebulit funcion

print("\n\nSecond round")
number1 = int(input("\nInsert the first number: "))
number2 = int(input("\nInsert the second number: "))
number3 = int(input("\nInsert the third number: "))
largest_number = max (number1, number2, number3)
print("The largest number is" , largest_number)