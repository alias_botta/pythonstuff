my_list = []  # we declare an empty list to sort
for i in range(int(input("Please tell me the amount of number that I have to read: "))):
    my_list.append(int(input("Insert the " + str(i) + " number: ")))
swapped = True  # It's a little fake, we need it to enter the while loop.

while swapped:
    swapped = False  # no swaps so far
    for i in range(len(my_list) - 1):#we decrement the list lenght because we use the incremented index in the for loop
        if my_list[i] > my_list[i + 1]:
            swapped = True  # a swap occurred!
            print("I'm going to swap " + str(my_list[i]) + " and " + str(my_list[i + 1]))#uncoment if u want a clean function
            my_list[i], my_list[i + 1] = my_list[i + 1], my_list[i]

print(my_list)
