# Your program must:
# ask the user to enter a word;
# use user_word = user_word.upper() to convert the word entered by the user to upper case; we'll talk about the so-called string methods and the upper() method very soon - don't worry;
# use conditional execution and the continue statement to "eat" the following vowels A, E, I, O, U from the inputted word;
# assign the uneaten letters to the word_without_vowels variable and print the variable to the screen.
# Look at the code in the editor. We've created word_without_vowels and assigned an empty string to it. Use concatenation operation to ask Python to combine selected letters into a longer string during subsequent loop turns, and assign it to the word_without_vowels variable.


word_without_vowels = ""
user_word = input("Please enter a word: ")
user_word = user_word.upper()
for letter in user_word:
    if (letter != 'A')  and (letter != 'E') and (letter != 'I') and (letter != 'O') and (letter != 'U'): 
        word_without_vowels += letter
    else:
        continue
print(word_without_vowels)
