import random

def display_board(board):
    # The function accepts one parameter containing the board's current status
    # and prints it out to the console.
    print("+-------+-------+-------+\
        \n|       |       |       |\
        \n|  ", board[0][0], "  |  ", board[0][1], "  |  ", board[0][2], "  |\
        \n|       |       |       |\
        \n+-------+-------+-------+\
        \n|       |       |       |\
        \n|  ", board[1][0], "  |   X   |  ", board[1][2], "  |\
        \n|       |       |       |\
        \n+-------+-------+-------+\
        \n|       |       |       |\
        \n|  ", board[2][0], "  |  ", board[2][1], "  |  ", board[2][2], "  |\
        \n|       |       |       |\
        \n+-------+-------+-------+")

def enter_move(board):
    # The function accepts the board current status, asks the user about their move, 
    # checks the input and updates the board according to the user's decision.
    while True:  
        move = int(input("Enter your move: "))
        i = 0
        while move > 3:
            move -=3
            i+=1
        exist = True
        try: 
            print("Column", i, "Row", move-1)
            exist = free_fields[i][move-1]
        except IndexError:
            print("The tried move doesn't exist.")
            exist = False  
        if exist == True: break
    board[i][move-1] = 'O'
    return board  
    
def make_list_of_free_fields(board):
    # The function browses the board and builds a list of all the free squares; 
    # the list consists of tuples, while each tuple is a pair of row and column numbers.
    free_fields = []
    for i in range(rows): 
        for j in range(columns): 
            if board[i][j] not in ('X', 'O'): free_fields.append((i,j))
    print("Free fields:", free_fields)
    return free_fields
    


def victory_for(board, sign):
    # The function analyzes the board status in order to check if 
    # the player using 'O's or 'X's has won the game

    #VERTICAL TEST
    for i in range(rows): 
        # first_face = board [i][0]
        if board[i][0] == sign: victory = True
        for j in range(columns): 
            if board[i][j] != sign: victory = False 
        if victory == True: return victory

    #HORIZONTAL TEST
    for i in range(columns): 
        # first_face = board [i][0]
        if board[i][0] == sign: victory = True
        for j in range(rows): 
            if board[i][j] != sign: victory = False 
        if victory == True: return victory

    if rows != columns: return None#we need an equilateral square for performing the oblique test
    #OBLIQUE TEST (top left to bottom right)
    if board[0][0] == sign: victory = True
    for i in range(rows): 
        if board[i][i] != sign: victory = False
    if victory == True: return victory

    #OBLIQUE TEST (bottom right to top left)
    if board[0][0] == sign: victory = True
    for i in reversed(range(columns)):
        for j in range(rows): 
            if board[i][j] != sign: victory = False  
    if victory == True: return victory



def draw_move(board):
    while True: 
        column = random.randint(0,2)
        row = random.randint(0,2)
        print("Sto cercando di idovinare..")
        print("Colonna =", column, "\nRiga=", row)
        if board[column][row] in free_fields: 
            print("Ho indovinato una posizione giusta!")
            board[column][row] = 'X'
            break
    return board

#MAIN START HERE

rows = columns = 3

board = [[1, 2, 3], [4, 'X', 6], [7, 8, 9]]
turn = 'O'# variable that determine who has to make a move

while not victory_for(board, 'X') and not victory_for(board, 'O'):
    free_fields = make_list_of_free_fields(board)
    display_board(board)
    if turn == 'O': 
        board = enter_move(board)
        turn = 'X'
    elif turn == 'X': 
        exit()
        board = draw_move(board)
        turn = 'O'

if victory_for(board, 'X'): print("The enemy won!")
else: print("You won!")
    


    
    