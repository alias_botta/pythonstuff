# Your task is to write and test a function which takes three arguments (a year, a month, and a day of the month) and returns the corresponding day of the year, or returns None if any of the arguments is invalid.
def is_year_leap(year):
	if (year % 4) == 0:  
		if (year % 100) == 0:  
			if (year % 400) == 0: return True#leap year
			else: return False#non leap year
		else: return True#leap year
	else: return False#non leap year

def days_in_month(year, month):
    days = [31, [28, 29], 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if month == 2 and is_year_leap(year) == False: return days[1][0] 
    elif month == 2 and is_year_leap(year) == True: return days[1][1]
    else: return days[month-1] 

def day_of_year(year, month, day):
    if month < 0 or month > 12: return None
    if day >= 0 and day <= days_in_month(year, month): day_result = day
    else: return None
    for i in range(month-1): day_result += days_in_month(year, i+1)
    return day_result

print(day_of_year(2000, 12, 31))
