secret_number = 777

#we're going to use multi line printing for that print function, you can implement in by inserting three quotes
print(
"""
+================================+
| Welcome to my game, muggle!    |
| Enter an integer number        |
| and guess what number I've     |
| picked for you.                |
| So, what is the secret number? |
+================================+
""")
while int(input("Print the secret number: ")) != secret_number:
    print ("Ha ha! You're stuck in my loop!")
print ("Well done, muggle! You are free now.")
