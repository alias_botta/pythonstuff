# You already know how split() works. Now we want you to prove it.

# Your task is to write your own function, which behaves almost exactly like the original split() method, i.e.:

# it should accept exactly one argument - a string;
# it should return a list of words created from the string, divided in the places where the string contains whitespaces;
# if the string is empty, the function should return an empty list;
# its name should be mysplit()
# Use the template in the editor. Test your code carefully.


def mysplit(strng):
    list = []
    word = ""
    for i in range(len(strng)):
        if not strng[i].isspace(): 
            word += strng[i]
        elif strng[i].isspace() and word == '': continue
        elif strng[i].isspace() and word != '':
            list.append(word)
            word = ""
        else: word += strng[i]
    if word != "": list.append(word)
    return list


print(mysplit("To be or not to be, that is the question"))
print(mysplit("To be or not to be,that is the question"))
print(mysplit("   "))
print(mysplit(" abc "))
print(mysplit(""))
