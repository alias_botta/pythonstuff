#!/usr/bin/env python3
#print("    *")
#print("   * *")
#print("  *   *")
#print(" *     *")
#print("***   ***")
#print("  *   *")
#print("  *   *")
#print("  *****")

## FIRST REQUEST
#print ("    *", "   * *", "  *   *", " *     *", "***   ***", "  *   *", "  *   *", "  *****", sep="\n")

## SECOND REQUEST
#print("       *")
#print("      * *")
#print("     *   *")
#print("    *     *")
#print("   *       *")
#print("  *         * ")
#print(" ***       ***")
#print("  *         *")
#print("  *         *")
#print("  ***********")

## THIRD REQUEST
#print(*    *    "*2)
#print("   * *   "*2)
#print("  *   *  "*2)
#print(" *     * "*2)
#print("***   ***"*2)
#print("  *   *  "*2)
#print("  *   *  "*2)
#print("  *****  "*2)

## FOURTH REQUEST
#print(*    *    *2)
#print("   * *   "*2)
#print("  *   *  "*2)
#print(" *     * "*2)
#print("***   ***"*2)
#print("  *   *  "*2)
#print("  *   *  "*2)
#print("  *****  "*2)

## FIFTH REQUEST
#print"*    *    "*2
#print("   * *   "*2)
#print("  *   *  "*2)
#print(" *     * "*2)
#print("***   ***"*2)
#print("  *   *  "*2)
#print("  *   *  "*2)
#print("  *****  "*2)

## SIXTH REQUEST
#Print("*    *    "*2)
#print("   * *   "*2)
#print("  *   *  "*2)
#print(" *     * "*2)
#print("***   ***"*2)
#print("  *   *  "*2)
#print("  *   *  "*2)
#print("  *****  "*2)

## SEVENTH REQUEST
#print(""    *    "*2)
#print("   * *   "*2)
#print("  *   *  "*2)
#print(" *     * "*2)
#print("***   ***"*2)
#print("  *   *  "*2)
#print("  *   *  "*2)
#print("  *****  "*2)

