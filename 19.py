# Imagine a list - not very long, not very complicated, just a simple list containing some integer numbers. Some of these numbers may be repeated, and this is the clue. We don't want any repetitions. We want them to be removed.

# Your task is to write a program which removes all the number repetitions from the list. The goal is to have a list in which all the numbers appear not more than once.

# Note: assume that the source list is hard-coded inside the code - you don't have to enter it from the keyboard. Of course, you can improve the code and add a part that can carry out a conversation with the user and obtain all the data from her/him.

# Hint: we encourage you to create a new list as a temporary work area - you don't need to update the list in situ.

original_list = [4, 4, 6, 3, 88, 6, 9, 66, 66, 88]
print("The original list is the following: " + str(original_list))
cleaned_list = []
for i in range(len(original_list)-1):
    if original_list[i] in original_list[i+1:]: 
        print("I'm going to delete " + str(original_list[i]))
    else:
        cleaned_list.append(original_list[i])
        print("I'm going to append " + str(original_list[i]))
print ("I've cleaned the list from the redundant numbers!\n" + str(cleaned_list))

