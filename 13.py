# Your task here is very special: you must design a vowel eater! Write a program that uses:

# a for loop;
# the concept of conditional execution (if-elif-else)
# the continue statement.
# Your program must:

# ask the user to enter a word;
# use user_word = user_word.upper() to convert the word entered by the user to upper case; we'll talk about the so-called string methods and the upper() method very soon - don't worry;
# use conditional execution and the continue statement to "eat" the following vowels A, E, I, O, U from the inputted word;
# print the uneaten letters to the screen, each one of them on a separate lin

# Prompt the user to enter a word
# and assign it to the user_word variable.

user_word = input("Please enter a word: ")
user_word = user_word.upper()
for letter in user_word:
    if (letter != 'A')  and (letter != 'E') and (letter != 'I') and (letter != 'O') and (letter != 'U'): 
        print(letter, end=' ')
    else:
        continue

    
