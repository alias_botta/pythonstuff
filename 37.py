# Your task is to slightly extend the Queue class' capabilities. We want it to have a parameterless method that returns True if the queue is empty and False otherwise.

# Complete the code we've provided in the editor. Run it to check whether it outputs a similar result to ours.

class QueueError(IndexError):
    pass


class Queue:
    def __init__(self):
        self.queue  = []

    def put(self, val):
        self.queue.append(val)

    def get(self):
        if len(self.queue) == 0: raise QueueError
        val = self.queue[0]
        del self.queue[0]
        return val


class SuperQueue(Queue):
    def __init__(self): 
        Queue.__init__(self)

    def isempty(self): 
        if len(self.queue) == 0: return True
        else: return False

que = SuperQueue()
que.put(1)
que.put("dog")
que.put(False)
for i in range(4):
    if not que.isempty():
        print(que.get())
    else:
        print("Queue empty")


