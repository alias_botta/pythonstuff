# The previous code needs to be improved. It's okay, but it has to be better.

# Your task is to make some amendments, which generate the following results:

# the output histogram will be sorted based on the characters' frequency (the bigger counter should be presented first)
# the histogram should be sent to a file with the same name as the input one, but with the suffix '.hist' (it should be concatenated to the original name)
# Assuming that the input file contains just one line filled with:

# cBabAa
# samplefile.txt

# the expected output should look as follows:

# a -> 3
# b -> 2
# c -> 1
# output

# Tip: Use a lambda to change the sort order.

from os import strerror

try:
    letters_found = {} #we're going to fill this dictionary soon
    filename = input("Enter the file name that I have to open: ")
    s = open((filename), 'rt')
    lines = s.readlines(20)
    while len(lines) != 0:
        for line in lines:
            line = line.lower()
            for ch in line:
                if not ch.isalpha(): continue
                if ch not in letters_found.keys(): letters_found[ch] = 1
                else: letters_found[ch]+=1
        lines = s.readlines(10)
    s.close()
except IOError as e:
    print("I/O error occurred:", strerror(e.errno))

#sorting the dictionary
letters_found = {k: v for k, v in sorted(letters_found.items(), key=lambda item: item[1], reverse=True)}

try:
    filename += ".hist"
    fo = open('output.txt', 'wt')
    for key, value in letters_found.items(): print(key, " -> ", value)
    fo.close()
except IOError as e:
    print("I/O error occurred: ", strerror(e.errno))