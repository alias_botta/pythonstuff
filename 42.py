# A text file contains some text (nothing unusual) but we need to know how often (or how rare) each letter appears in the text. Such an analysis may be useful in cryptography, so we want to be able to do that in reference to the Latin alphabet.

# Your task is to write a program which:

# asks the user for the input file's name;
# reads the file (if possible) and counts all the Latin letters (lower- and upper-case letters are treated as equal)
# prints a simple histogram in alphabetical order (only non-zero counts should be presented)
# Create a test file for the code, and check if your histogram contains valid results.

# Assuming that the test file contains just one line filled with:

# aBc
# samplefile.txt

# the expected output should look as follows:

# a -> 1
# b -> 1
# c -> 1
# output

# Tip: We think that a dictionary is a perfect data collection medium for storing the counts. The letters may be keys while the counters can be values.
from os import strerror

try:
    letters_found = {} #we're going to fill this dictionary soon
    s = open(input("Enter the file name that I have to open: "), 'rt')
    lines = s.readlines(20)
    while len(lines) != 0:
        for line in lines:
            line = line.lower()
            for ch in line:
                if not ch.isalpha(): continue
                if ch not in letters_found.keys(): letters_found[ch] = 1
                else: letters_found[ch]+=1
        lines = s.readlines(10)
    s.close()
except IOError as e:
    print("I/O error occurred:", strerror(e.errno))

try:
    fo = open('output.txt', 'wt')
    for key, value in letters_found.items(): print(key, " -> ", value)
    fo.close()
except IOError as e:
    print("I/O error occurred: ", strerror(e.errno))
