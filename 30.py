# An anagram is a new word formed by rearranging the letters of a word, using all the original letters exactly once. For example, the phrases "rail safety" and "fairy tales" are anagrams, while "I am" and "You are" are not.

# Your task is to write a program which:

# asks the user for two separate texts;
# checks whether, the entered texts are anagrams and prints the result.
# Note:

# assume that two empty strings are not anagrams;
# treat upper- and lower-case letters as equal;
# spaces are not taken into account during the check - treat them as non-existent
# Test your code using the data we've provided.

def are_anagrams(string1, string2): 
    if string1 == '' and string2 == '': return False
    string1, string2 = string1.replace(" ", ""), string2.replace(" ", "")
    string1, string2 = string1.upper(), string2.upper()
    string1, string2 = sorted(string1), sorted(string2)
    if string1 == string2: return True
    else: return False

if are_anagrams("Listen","Silent"): print("Anagrams")
else: print("Not anagrams")

if are_anagrams("modern","norman"): print("Anagrams")
else: print("Not anagrams")